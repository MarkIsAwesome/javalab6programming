import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField winCount;
    private TextField lossCount;
    private TextField tieCount;
    private String playerChoice;
    private RpsGame game;
    
    public RpsChoice(TextField message, TextField winCount, TextField lossCount, TextField tieCount, String playerChoice, RpsGame game) {
        this.message = message;
        this.winCount = winCount;
        this.lossCount = lossCount;
        this.tieCount = tieCount;
        this.playerChoice = playerChoice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e) {
        String result = this.game.playRound(this.playerChoice);
        this.message.setText(result);
        this.winCount.setText(String.valueOf(this.game.getWins()));
        this.lossCount.setText(String.valueOf(this.game.getLoss()));
        this.tieCount.setText(String.valueOf(this.game.getTies()));
    }

    //Mark Agluba
}
