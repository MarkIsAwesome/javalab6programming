import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int loss;
    private Random random;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.loss = 0;
        this.random = new Random();
    }

    public int getWins() {
        return wins;
    }


    public int getTies() {
        return ties;
    }

    public int getLoss() {
        return loss;
    }

    public String playRound(String choice) {
        int rand = this.random.nextInt(3);
        String result = "";
        if (rand == 0) {
            if (choice.equals("rock")) {
                this.ties++;
                result = "The AI pick rock and its a tie!";
            }
            else if(choice.equals("paper")) {
                this.loss++;
                result = "The AI pick rock and AI lost!";
            }
            else if(choice.equals("scissors")) {
                this.wins++;
                result = "The AI pick rock and AI wins!";
            }
        }
        else if (rand == 1) {
            if (choice.equals("rock")) {
                this.loss++;
                result = "The AI pick scissors and AI lost!";
            }
            else if(choice.equals("paper")) {
                this.wins++;
                result = "The AI pick scissors and AI wins!";
            }
            else if(choice.equals("scissors")) {
                this.ties++;
                result = "The AI pick scissors and its a tie!";
            }
        }
        else if (rand == 2) {
            if (choice.equals("rock")) {
                this.wins++;
                result = "The AI pick paper and the AI wins!";
            }
            else if(choice.equals("paper")) {
                this.ties++;
                result = "The AI pick paper and its a tie!";
            }
            else if(choice.equals("scissors")) {
                this.loss++;
                result = "The AI pick paper and the AI lost!";
            }
        }
        return result;
    }

}