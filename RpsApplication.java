
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	

	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 

        //creating buttons
        Button rockButton = new Button("rock");
        Button scissorsButton = new Button("scissors");
        Button paperButton  = new Button("paper");

        //creating the textFields
        TextField welcome = new TextField("Welcome!");
        TextField winCount = new TextField("wins: ");
        TextField lossCount = new TextField("losses: ");
        TextField tieCount = new TextField("ties: ");
        TextField name = new TextField("Mark Agluba");


        //adding HBox in the root
        HBox buttons = new HBox();
        buttons.getChildren().addAll(rockButton, scissorsButton, paperButton);

        //creating and adding an hbox to the root
        HBox textFields = new HBox();
        textFields.getChildren().addAll(welcome, winCount, lossCount, tieCount);

        welcome.setPrefWidth(200);

        //creating and adding vbox to the root
        VBox overall = new VBox();
        overall.getChildren().addAll(buttons, textFields, name);
        root.getChildren().addAll(overall);

        //setOnAction
        RpsGame game = new RpsGame();
        rockButton.setOnAction(new RpsChoice(welcome, winCount, lossCount, tieCount, "rock", game));
        scissorsButton.setOnAction(new RpsChoice(welcome, winCount, lossCount, tieCount, "scissors", game));
        paperButton.setOnAction(new RpsChoice(welcome, winCount, lossCount, tieCount, "paper", game));
        //Mark Agluba
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

